#include <iostream>
#include "backend.h"
using namespace std;

Backend::Backend () {};

Backend::~Backend () {};

void Backend::Cut (const int start, const int stop) {
    // se apleaza daca textul nu este gol si daca avem un index de stop pozitiv
    if (text.isEmpty() != 1 && stop > 0) {
        if (clipboard.isEmpty())
            text.cut(start,stop,clipboard);
        else {
            while (clipboard.isEmpty() != 1) {
                clipboard.removeFirst();
            }
            text.cut(start,stop,clipboard);
        }
        int i, dim = clipboard.dimensiune();
        // declar un element de tip elem si il adaug in stiva de undo
        elem add;
        add.tip_op = 3;
        add.repeat = dim;
        add.start = start;
        add.stop = stop;
        for (i = 0; i < dim; i++) {
            add.cuv = clipboard.val(i);
            undo.push(add);
        }
    }
}
void Backend::Copy (const int start, const int stop) {
    // se apleaza daca textul nu este gol si daca avem un index de stop pozitiv
    if (text.isEmpty() != 1 && stop > 0) {
        if (clipboard.isEmpty()) {
            text.copy(start,stop,clipboard);
        }
        else {
            while (clipboard.isEmpty() != 1) {
                clipboard.removeFirst();
            }
            text.copy(start,stop,clipboard);
        }
    }

}
void Backend::Paste(const int start) {
    // se apeleaza numai daca se afla cuvinte in clipboard
    if (clipboard.isEmpty() != 1) {
        string s;
        int dim, i, indice;
        dim = clipboard.dimensiune();
        elem add;
        add.repeat = dim;
        add.tip_op = 2;
        if (start != 0)
          indice = text.indice_paste(start);
        else
            indice = start;
        for (i = 0; i < dim; i++) {
            s = clipboard.val(i);
            add.start = indice;
            add.cuv = s;
            undo.push(add);
            Add(indice,s.c_str());
            indice += s.length();
        }
    }
}
void Backend::Undo (void) {
    // se apeleaza numai daca exista ceva in stiva
    if (undo.isEmpty() != 1) {
        elem x;
        x = undo.pop();
        if (x.tip_op == 1) {
            text.del(x.start,x.start + (x.cuv).length());
            redo.push(x);
        }
        else if (x.tip_op == 2) {
            int i, contor = x.repeat;
            text.del(x.start, x.start + (x.cuv).length());
            redo.push(x);
            for (i = 0; i < contor -1 ; i++) {
                x = undo.pop();
                text.del(x.start, x.start + (x.cuv).length());
                redo.push(x);
            }
        }
        else {
            int i, l, contor = x.repeat;
            Add(x.start,x.cuv.c_str());
            l = x.cuv.length();
            redo.push(x);
            for (i = 0; i < contor - 1 ; i++) {
                x = undo.pop();
                Add(x.start - l,x.cuv.c_str());
                l = x.cuv.length();
                redo.push(x);
            }
        }
    }
}
void Backend::Redo (void) {
    // se apeleaza numai daca exista ceva in stiva
    if (redo.isEmpty() != 1) {
        elem x;
        x = redo.pop();
        if (x.tip_op == 1) {
            Add(x.start,x.cuv.c_str());
            undo.push(x);
        }
        else if (x.tip_op == 2) {
            int i, contor = x.repeat;
            Add(x.start,x.cuv.c_str());
            undo.push(x);
            for (i = 0; i < contor - 1 ; i++) {
                x = redo.pop();
                Add(x.start,x.cuv.c_str());
                undo.push(x);
            }
        }
        else {
            int i, contor = x.repeat;
            Cut(x.start,x.stop);
            undo.push(x);
            for (i = 0; i < contor - 1; i++) {
                x =redo.pop();
                undo.push(x);
            }
        }
    }
}
void Backend::Add (const int index, const char *word) {
	string cuv = word;
	int ok = 0;
	if (cuv[cuv.length()-1] != ' ') {
        cuv.append(1,' ');
        ok = 1;
    }
	if (ok == 1) {
        elem add;
        add.start = index;
        add.cuv = cuv;
        add.tip_op = 1;
        undo.push(add);
        while (redo.isEmpty() != 1)
            redo.pop();
	}
	if (text.isEmpty()) {
        text.addLast(cuv);
	}
	else {
        if (index >= text.size()) {
            text.addLast(cuv);
        }
        else {
            if (index <= 0) {
                text.addFirst(cuv);
            }
            else {
                text.addMiddle(cuv,index);
            }
        }
	}
}
const char *Backend::GetText (void) {
    string afis;
    if (text.size() > 0) {
            int i, dim;
            dim = text.dimensiune();
            afis = text.val(0);
            for (i = 1; i < dim; i++) {
                afis.append(text.val(i));
            }
            return afis.c_str();
    }
    else {
        afis = "";
        return afis.c_str();
    }
}
