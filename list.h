#ifndef LISTA_SIMPLA_H_INCLUDED
#define LISTA_SIMPLA_H_INCLUDED
using namespace std;

template<typename T>
struct nod {
    T val;
    nod *next;
};

template<typename T>
class List {
    nod<T> *pFirst,*pLast;
    int dim;
public:
    List () {
        pFirst = pLast = NULL;
        dim = 0;
    }
    // adauga un element la inceputul listei
    void addFirst(T x) {
        nod<T> *p = new nod<T>;
        p->val = x;
        dim++;
        if (isEmpty()) {
            pFirst = pLast = p;
            pLast->next = NULL;
        }
        else {
            p->next = pFirst;
            pFirst = p;
        }
    }
    // adauga un element la sfarsitul listei
    void addLast (T x) {
        nod<T> *p = new nod<T>;
        p->val = x;
        dim++;
        if (isEmpty()) {
            pFirst = pLast = p;
            p->next = NULL;
        }
        else {
            pLast->next = p;
            pLast = p;
            pLast->next = NULL;
        }
    }
    // sterge un element de la inceputul listei
    T removeFirst () {
        if (isEmpty()) {
            cout<<"Error removeFirst - Lista este vida!"<<endl;
            return T();
        }
        else {
            T x;
            nod<T> *p;
            p = pFirst;
            dim--;
            if (p->next != NULL) {
                pFirst = p->next;
                x = p->val;
                delete p;
                return x;
            }
            else {
                pFirst = pLast = NULL;
                x = p->val;
                delete p;
                return x;
            }
        }
    }
    // sterge un element de la sfarsitul listei
    T removeLast () {
        if (isEmpty()) {
            cout << "Error removeLast - Lista este vida!" << endl;
            return 0;
        }
        else {
            T x;
            nod<T> *p;
            p = pFirst;
            dim--;
            if (p->next != NULL) {
                nod<T> *aux;
                for (p = pFirst; p != pLast; p = p->next)
                        aux = p;
                aux->next = NULL;
                pLast = aux;
                x = p->val;
                delete p;
                return x;
            }
            else {
                pFirst = pLast = NULL;
                x = p->val;
                delete p;
                return x;
            }
        }
    }
    // insereaza un element intre doua noduri din lista
    void addMiddle (T x, int ind) {
        nod<T> *p;
        int c = 0;
        dim++;
        for (p = pFirst; p != NULL; p = p->next) {
            c += p->val.length();
            if (c - 1 == ind || c == ind) {
                p = p->next;
                nod<T> *l = new nod<T>;
                l->val = p->val;
                p->val = x;
                if (p->next == NULL) {
                    pLast = l;
                    pLast->next = NULL;
                    p->next = pLast;
                }
                else {
                    l->next = p->next;
                    p->next = l;
                }
                break;
            }
            if (ind < c) {
                nod<T> *l = new nod<T>;
                l->val = x;
                if (p->next == NULL) {
                    pLast = l;
                    pLast->next = NULL;
                    p->next = pLast;
                }
                else {
                    l->next = p->next;
                    p->next = l;
                }
                break;
            }
        }
    }
    // sterge un element din lista
    void removeMiddle (nod<T> *p) {
        nod<T> *l;
        if (p == pFirst) {
            removeFirst();
        }
        else if (p == pLast) {
            removeLast();
        }
        else {
            dim--;
            l = p->next;
            p->val = l->val;
            if (l->next == NULL) {
                pLast = p;
                pLast->next = NULL;
                delete l;
            }
            else {
                p->next = l->next;
                delete l;
            }
        }
    }
    // copiaza toate cuvintele cuprinse integral intre start si stop
    void copy (int start, int stop, List<T> &clip) {
        nod<T> *p;
        if (start <= 1 && stop >= size()) {
            for (p = pFirst; p != NULL; p = p->next) {
                clip.addLast(p->val);
            }
        }
        else if (dim == 1 && stop >= size() && start <= 1)
            clip.addLast(pFirst->val);
        else {
            int c = 0;
            for (p = pFirst; p != NULL; p = p->next) {
                c += p->val.length();
                if ( c >= start) {
                    if (start <= 1 && stop >= c) {
                        clip.addLast(p->val);
                    }
                    while (c < stop) {
                        p = p->next;
                        if (p == NULL)
                            break;
                        c += p->val.length();
                        if (c <= stop) {
                            clip.addLast(p->val);
                        }
                    }
                    break;
                }
            }
        }
    }
    /* 
    * copiaza in clipboard toate cuvintele cuprinse integral intre start 
    *  si stop si le sterge din text
    */
    void cut (int start, int stop, List<T> &clip) {
        nod<T> *p;
        if (start <= 1 && stop >= size()) {
            for (p = pFirst; p != NULL; p = p->next) {
                clip.addLast(p->val);
                removeMiddle(p);
            }
        }
        else if (dim == 1 && stop >= size() && start <= 1) {
            clip.addLast(pFirst->val);
            removeMiddle(pFirst);
        }
        else {
            int c = 0;
            nod<T> *l;
            for (p = pFirst; p != NULL; p = l) {
                l = p->next;
                c += p->val.length();
                if ( c >= start) {
                    if (start <= 1 && stop >= c) {
                        clip.addLast(p->val);
                        removeMiddle(p);
                        continue;
                    }
                    p = p->next;
                    while (c < stop) {
                        if (p == NULL)
                            break;
                        c += p->val.length();
                        if (c <= stop) {
                            clip.addLast(p->val);
                            if (p->next == NULL) {
                                removeMiddle(p);
                                break;
                            }
                            else
                                removeMiddle(p);
                        }
                    }
                    break;
                }
            }
        }
    }
    // sterge din text cuvintele cuprinse integral intre start si stop
    void del (int start, int stop) {
        nod<T> *p;
        if (start <= 1 && stop >= size()) {
            for (p = pFirst; p != NULL; p = p->next) {
                removeMiddle(p);
            }
        }
        else if (dim == 1 && stop >= size() && start <= 1) {
            removeMiddle(pFirst);
        }
        else {
            int c = 0;
            nod<T> *l;
            for (p = pFirst; p != NULL; p = l) {
                l = p->next;
                c += p->val.length();
                if ( c >= start) {
                    if (start <= 1 && stop >= c) {
                        removeMiddle(p);
                        continue;
                    }
                    p = p->next;
                    while (c < stop) {
                        if (p == NULL)
                            break;
                        c += p->val.length();
                        if (c <= stop) {
                            if (p->next == NULL) {
                                removeMiddle(p);
                                break;
                            }
                            else
                                removeMiddle(p);
                        }
                    }
                    break;
                }
            }
        }
    }
    // returneaza dimensiunea listei
    const int dimensiune () {
        return dim;
    }
    // returneaza dimensiunea toatal a textului (nr. de caractere)
    const int size () {
        nod<T> *p;
        int c = 0;
        for (p = pFirst; p != NULL; p = p->next)
            c += p->val.length();
        return c;
    }
    // returneaza valoare nodului i din lista
    T val (int i) {
        nod<T> *p;
        for (p = pFirst; i != 0; p = p->next)
            i--;
        return p->val;
    }
    // returneaza un indice valid pt. functia paste
    int indice_paste (int start) {
        nod<T> *p;
        int c = 0;
        for (p = pFirst; p != NULL; p = p->next) {
            c += p->val.length();
            if (c >= start)
                return c;
        }
        return size();
    }
    // verifica daca lista este goala
    int isEmpty () {
        return (pFirst == NULL && pLast == NULL);
    }
    ~List () {
        nod<T> *p,*l;
        for (p = pFirst; p != NULL; p = l) {
            l = p->next;
            delete p;
        }
    }
};

#endif
