#ifndef STIVA_H_INCLUDED
#define STIVA_H_INCLUDED
#include "list.h"

template<typename T>
class Stack {
    List<T> a;
public:
    Stack () {};
    void push (T x) {
        a.addFirst(x);
    }
    T pop () {
        if (isEmpty()) {
            cout << "Eroare pop - Stiva este goala!" << endl;
            return T();
        }
        else
            return a.removeFirst();
    }
    T peek () {
        if (isEmpty()) {
            cout << "Eroare peek - Stiva este goala!" << endl;
            return T();
        }
        else{
            T x;
            x = a.removeFirst();
            a.addFirst(x);
            return x;
        }
    }
    int isEmpty() {
        return a.isEmpty();
    }
};

#endif
